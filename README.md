# Asterisk Web Manager
API y Web para la administración de asterisk (coming soon)

## Variables de entorno

`AMI_USERNAME`						-> Nombre de usuario para autenticación a AMI

`AMI_SECRET`							-> Contraseña para autenticación a AMI

`ASTERISK_HOST`						-> Hostname / IP de Asterisk

`ASTERISK_SIP_TRUNK_FILE` -> Destino de las troncales SIP configuradas (por defecto `sip.conf`)

`ASTERISK_IAX_TRUNK_FILE` -> Destino de las troncales SIP configuradas (por defecto `iax.conf`)

`ASTERISK_SOUNDS_FOLDER`	-> Carpeta que contiene los audios de asterisk. Por defecto: `data/sounds`

`ASTERISK_DIALPLAN_FILE`	-> Archivo de configuración donde se almacenarán los cambios realizados en el dialplan: por defecto `extensions.conf` 

