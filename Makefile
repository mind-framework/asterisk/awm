build:
	go mod vendor
	go build

install: build
	go install

dev: install
	cd suites/dev; \
	docker-compose up -d
	reflex -s -r '\.go$$' -- sh -c "go build && go install && awm"
dev-web:
	cd web; \
	yarn install; \
	yarn serve

dev-stop:
	cd suites/dev; \
	docker-compose down --rmi=local
