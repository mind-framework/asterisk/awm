import Vue from 'vue';
const maxEvents = 1000

var state = Vue.observable({ 
  evtSource: null,
  eventList:[]
});

state.clearEvents = function() {
  state.eventList = []
}

state.Connect = function() {
  state.evtSource = new EventSource("api/events");
  state.evtSource
  state.evtSource.onmessage = (event) => {
    const data = JSON.parse(event.data)

    if(data.Event) {
      const ts = new Date()
      const e = data.Event
      delete data.Event
      // data.

      if(state.eventList.length > maxEvents) {
        state.eventList.shift()
      }
      state.eventList.push({
        Event: e,
        Timestamp: `${ts.getFullYear()}-${ts.getMonth()+1}-${ts.getDate()} ${ts.getHours()}:${ts.getMinutes()}:${ts.getSeconds()}.${ts.getMilliseconds()}`,
        Details: data,
      })

    }
  }
}


export default state

