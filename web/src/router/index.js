import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Home,
		meta: {
			view: {
				title: 'Inicio',
			},
			nav: {
				icon: 'el-icon-s-home',
			},
		}
  },
  {
		path: '/trunks',
		name: 'Troncales',
		component: () => import(/* webpackChunkName: "trunks" */ '../views/Trunks.vue'),
		meta: {
			view: {
				title: 'Administración de Troncales',
			},
			nav: {
				menu: "Telefonía",
				icon: 'el-icon-share',
				menuIcon: 'el-icon-phone-outline',
			},
		},
		children:[
			{
				path: ':type/:id',
				component: () => import(/* webpackChunkName: "trunks" */ '../views/Trunk.vue'),
				meta: {
					view: {
						title: 'Propiedades de la troncal',
					},
				}
			},
			{
				path: 'new',
				component: () => import(/* webpackChunkName: "trunks" */ '../views/Trunk.vue'),
				meta: {
					view: {
						titleNew: 'Nueva Troncal',
					},
				}
			}
		]
  },
  {
		path: '/extensions',
		name: 'Extensiones',
		component: () => import(/* webpackChunkName: "users" */ '../views/Users.vue'),
		meta: {
			view: {
				title: 'Administración de Extensiones',
			},
			nav: {
				menu: "Telefonía",
				icon: 'el-icon-video-play',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "users" */ '../views/User.vue'),
				meta: {
					view: {
						title: 'Propiedades',
					},
				}
			},
		]
	},
  {
		path: '/sounds',
		name: 'Audios',
		component: () => import(/* webpackChunkName: "trunks" */ '../views/Sounds.vue'),
		meta: {
			view: {
				title: 'Administración de Audios',
			},
			nav: {
				menu: "Recursos",
				icon: 'el-icon-video-play',
				menuIcon: 'el-icon-s-cooperation',
			},
		},
		// children:[
		// 	{
		// 		path: ':id',
		// 		component: () => import(/* webpackChunkName: "trunks" */ '../views/Trunk.vue'),
		// 		meta: {
		// 			view: {
		// 				title: 'Propiedades del Audio',
		// 			},
		// 		}
		// 	},
		// ]
	},
  {
		path: '/playlists',
		name: 'Play Lists',
		component: () => import(/* webpackChunkName: "playlist" */ '../views/PlayLists.vue'),
		meta: {
			view: {
				title: 'Administración de PlayLists',
			},
			nav: {
				menu: "Recursos",
				icon: 'el-icon-share',
				menuIcon: 'el-icon-share',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "playlist" */ '../views/PlayList.vue'),
				meta: {
					view: {
						title: 'Editar PlayList',
					},
				}
			},
		]
	},
	{
		path: '/modulos',
		name: 'Módulos',
		component: () => import(/* webpackChunkName: "modules" */ '../views/Modules.vue'),
		meta: {
			view: {
				title: 'Módulos Asterisk',
			},
			nav: {
				icon: 'el-icon-cpu',
				menu: 'Avanzado',
				menuIcon: 'el-icon-setting',
			},
		},
  },
  {
		path: '/dialplan',
		name: 'Dialplan',
		component: () => import(/* webpackChunkName: "modules" */ '../views/Dialplan.vue'),
		meta: {
			view: {
				title: '',
			},
			nav: {
				icon: 'el-icon-phone',
				menu: 'Avanzado',
			},
		},
		children:[
			{
				path: ':id',
				component: () => import(/* webpackChunkName: "playlist" */ '../views/DialplanContext.vue'),
				meta: {
					view: {
						title: 'Editar Contexto de Dialplan',
						titleNew: 'Crear Contexto de Dialplan',
					},
				}
			},
		]
  },
  {
		path: '/config-files',
		name: 'Editor de Config.',
		component: () => import(/* webpackChunkName: "modules" */ '../views/ConfigView.vue'),
		meta: {
			view: {
				title: 'Editor de Configuración',
			},
			nav: {
				icon: 'el-icon-setting',
				menu: 'Avanzado',
			},
		},
  },
  {
		path: '/eventos',
		name: 'Eventos',
		component: () => import(/* webpackChunkName: "events" */ '../views/Events.vue'),
		meta: {
			view: {
				// title: 'Administración de Clientes',
			},
			nav: {
				icon: 'el-icon-warning-outline',
				menu: 'Avanzado',
			},
		},
  },
]

const router = new VueRouter({
  routes
})

export default router
