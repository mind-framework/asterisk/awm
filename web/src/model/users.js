import Model from '@mind-core-vue/helper/model'

class Modelo extends Model {
	constructor() {
		super({
			uri:'/api/ami/users',
			key: 'id',
			template: {
				id: null,
			}
		})
		this.refreshTimeout = 1
	}
}

export default new Modelo()