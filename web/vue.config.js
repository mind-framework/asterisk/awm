const path = require('path');  
module.exports = {
	publicPath: './',
	devServer: {
		disableHostCheck: true,
		// https: true,
		port: 8080,
		proxy: {
			'/api': {
				target: (process.env.API_PROTOCOL || 'http') + '://' + (process.env.API_HOST || '127.0.0.1') + ':' + process.env.API_PORT,
				secure: false,
				// pathRewrite: { '^/api': '/awm/api' } 
			},
		}
	},
	configureWebpack: {
		plugins: [
		],
		resolve: {
			alias: {  
				'@mind-core-vue': path.resolve(__dirname, 'src/../modules/mind-core-vue/src'),  
			}
		},
	}
}