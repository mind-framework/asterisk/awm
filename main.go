package main

import (
	"context"
	"os"
	"path/filepath"
	"strings"
	// "time"

	// paquetes requeridos para mantejo de requests
	"net/http"

	"github.com/go-chi/chi"
	// "github.com/go-chi/chi/middleware"
	// "github.com/go-chi/render"
	_log "github.com/rs/zerolog/log"
	goami "gitlab.com/mind-framework/asterisk/goami/ami"

	//helpers para handlers y modelos
	_ "gitlab.com/mind-framework/core/mind-core-api/app"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	// "gitlab.com/mind-framework/core/mind-core-api/handler"
	// "gitlab.com/mind-framework/core/mind-core-api/model"

	//paquetes específicos del proyecto
	"gitlab.com/mind-framework/asterisk/awm/api/ami"
	"gitlab.com/mind-framework/asterisk/awm/api/events"
	"gitlab.com/mind-framework/asterisk/awm/api/sounds"
	"gitlab.com/mind-framework/asterisk/awm/eventsbroker"
)

var log = _log.With().Str("pkg", "main").Logger()

func main() {

	//obtengo los parámetros desde las variables de entorno
	apiPort := os.Getenv("API_PORT")
	if apiPort == "" {
		apiPort = "8080"
	}

	amiUsername := os.Getenv("AMI_USERNAME")
	if amiUsername == "" {
		log.Fatal().Msg("no se definió AMI_USERNAME")
		return
	}

	amiSecret := os.Getenv("AMI_SECRET")
	if amiSecret == "" {
		log.Fatal().Msg("no se definió AMI_SECRET")
		return
	}

	asteriskHost := os.Getenv("ASTERISK_HOST")
	if asteriskHost == "" {
		log.Fatal().Msg("no se definió ASTERISK_HOST")
		return
	}

	soundsFolder := os.Getenv("ASTERISK_SOUNDS_FOLDER")
	if soundsFolder == "" {
		soundsFolder = "/var/lib/asterisk/sounds"
	}

	log.Info().Str("soundsFolder", soundsFolder).Msg("")

	//defino un contexto
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	//Inicio el pool de conexiones a asterisk
	pool, err := goami.NewPool(ctx, asteriskHost, amiUsername, amiSecret, "system,call,all,user")
	if err != nil {
		log.Fatal().Err(err).Msg("Error al iniciar pool")
		return
	}
	defer pool.CloseAll()
	pool.MinConections = 1
	log.Info().Str("ASTERISK_HOST", asteriskHost).Msg("Conectando con Asterisk")
	if err := pool.Connect(); err != nil {
		log.Fatal().Err(err).Msg("Error al iniciar conexiones con asterisk")
	}
	log.Debug().Interface("pool", pool).Msg("")

	//Inicio broker de eventos
	log.Info().Msg("Iniciando Broker de Eventos")
	eventsBroker := eventsbroker.NewBroker()
	eventsBroker.Start(ctx, pool)
	// go func() {
	// 	for {
	// 		time.Sleep(2000 * time.Millisecond)
	// 		s, err := pool.GetSocket()
	// 		if err != nil {
	// 			log.Error().Err(err).Msg("Error al obtener un socket del pool")
	// 			continue
	// 		}
	// 		if err := goami.Ping(ctx, s, ""); err != nil {
	// 			log.Error().Err(err).Msg("Error en Ping")
	// 			pool.Close(s, true)
	// 			continue
	// 		}
	// 		pool.Close(s, false)
	// 	}
	// }()

	//Configuración de Rutas
	r := chi.NewRouter()
	// r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(middlewares.ParseParamsCtx)
	// r.Use(middleware.Logger)

	workDir, _ := os.Getwd()
	filesDir := http.Dir(filepath.Join(workDir, "web/dist"))
	FileServer(r, "/", filesDir)

	// r.Get("/", func(w http.ResponseWriter, r *http.Request) {
	// })
	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`pong`))
	})

	r.Mount("/api/events", events.NewHandler(ctx, eventsBroker).Routes())
	r.Mount("/api/ami", ami.NewHandler(ctx, pool).Routes())
	r.Mount("/api/sounds", sounds.NewHandler(ctx, soundsFolder).Routes())

	log.Info().Str("puerto", apiPort).Msg("Iniciando Listener")

	if err := http.ListenAndServe(":"+apiPort, r); err != nil {
		log.Fatal().Err(err).Str("cat", "http").Msg("No se pudo iniciar servicio http")
	}

}

//getDBConnectionString devuelve el connection string utilizando los datos recibidos en las variables de entorno
// func getDBConnectionString() (string, error) {
// 	log = log.With().Str("ctx", "getDBConnectionString").Logger()
// 	connStr := os.Getenv("DB_CONNECTION_STRING")
// 	if connStr == "" {
// 		dbHost := os.Getenv("DB_HOST")
// 		if dbHost == "" {
// 			return "", errors.New("DB_HOST no especificado")
// 		}
// 		log.Debug().Str("DB_HOST", dbHost).Msg("env")

// 		dbPort := os.Getenv("DB_PORT")
// 		if dbPort == "" {
// 			dbPort = "3306"
// 			log.Warn().Str("Puerto", dbPort).Msg("No se definió el puerto de conexión a BD, utilizando puerto por default")
// 		}
// 		log.Debug().Str("DB_PORT", dbPort).Msg("env")

// 		dbName := os.Getenv("API_DB_NAME")
// 		if dbName == "" {
// 			return "", errors.New("API_DB_NAME no especificado")
// 		}
// 		log.Debug().Str("API_DB_NAME", dbName).Msg("env")

// 		dbUserName := os.Getenv("API_DB_USERNAME")
// 		if dbUserName == "" {
// 			return "", errors.New("API_DB_USERNAME no especificado")
// 		}
// 		log.Debug().Str("API_DB_USERNAME", dbUserName).Msg("env")

// 		dbPassword := os.Getenv("API_DB_SECRET")
// 		if dbPassword == "" {
// 			return "", errors.New("API_DB_SECRET no especificado")
// 		}
// 		connStr = fmt.Sprintf("%s:%s@(%s:%s)/%s", dbUserName, dbPassword, dbHost, dbPort, dbName)
// 	}
// 	return connStr, nil
// }

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
