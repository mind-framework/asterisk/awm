package eventsbroker

/*
* El broker de eventos permite leer los eventos de asterisk y distribuirlos entre los subscriptores
* Esta implementación está basada en https://github.com/kljensen/golang-html5-sse-example/blob/master/server.go
 */

import (
	"context"
	"sync"

	_log "github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	"time"
)

var log = _log.With().Str("pkg", "eventbroker").Logger()

//NewBroker devuelve un nuevo EventsBroker de canales
func NewBroker() *EventsBroker {
	return &EventsBroker{
		clients:     make(map[chan ami.Response]bool),
		subscribe:   make(chan (chan ami.Response)),
		Unsubscribe: make(chan (chan ami.Response)),
		Publish:     make(chan ami.Response),
		stop:        make(chan struct{}),
		running:     false,
	}
}

// EventsBroker se encarga de leer los eventos AMI y distribuirlos entre
// los subscriptores
type EventsBroker struct {
	clients map[chan ami.Response]bool

	subscribe   chan chan ami.Response
	Unsubscribe chan chan ami.Response
	Publish     chan ami.Response
	mutex       sync.Mutex
	wg          sync.WaitGroup
	stop        chan struct{}
	running     bool
}

//Subscribe returns a new event channel
func (b *EventsBroker) Subscribe() chan ami.Response {
	messageChan := make(chan ami.Response, 10)
	b.subscribe <- messageChan
	return messageChan
}

// Start This EventsBroker method starts a new goroutine.  It handles
// the addition & removal of clients, as well as the broadcasting
// of messages out to clients that are currently attached.
func (b *EventsBroker) Start(ctx context.Context, pool *ami.Pool) {
	go func() {
		for {
			select {
			case <-b.stop:
				return
			case <-ctx.Done():
				return
			case s := <-b.subscribe:
				b.clients[s] = true
				b.wg.Add(1)

			case s := <-b.Unsubscribe:
				if b.clients[s] {
					b.mutex.Lock()
					delete(b.clients, s)
					close(s)
					b.mutex.Unlock()
					b.wg.Done()
				}
			case msg := <-b.Publish:
				b.mutex.Lock()
				for s := range b.clients {
					s <- msg
				}
				b.mutex.Unlock()
			}
		}
	}()

	//funcion que recibe los eventos y los distribuye a traves de los suscriptores
	go func() {
		client, err := pool.GetSocket()
		if err != nil {
			log.Fatal().Err(err).Msg("Error al obtener conexión del pool ")
		}
		for {
			select {
			default:
				if client.Connected() {
					events, err := ami.Events(ctx, client)
					if err != nil {
						log.Error().Err(err).Msg("Error al leer eventos asterisk")
						pool.Close(client, true)
						for {
							time.Sleep(100 * time.Millisecond)
							client, err = pool.GetSocket()
							if err != nil {
								log.Error().Err(err).Msg("Error al obtener conexión del pool ")
								continue
							}
							break
						}
						continue
					}
					b.Publish <- events
				}
			}
		}
	}()
	b.running = true

}

//Stop Detiene el broker de eventos
func (b *EventsBroker) Stop(ctx context.Context) {
	b.wg.Wait()
	b.running = false
	close(b.stop)

}

//IsRunning returns true if the event broker is running
func (b *EventsBroker) IsRunning() bool {
	return b.running
}
