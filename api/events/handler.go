package events

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	// "gitlab.com/mind-framework/asterisk/goami/ami"
	_log "github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/asterisk/awm/eventsbroker"

	"net/http"
	"time"
)

var log = _log.With().Str("pkg", "sse").Logger()

//NewHandler devuelve una nueva instancia del handler para este recurso
func NewHandler(ctx context.Context, events *eventsbroker.EventsBroker) *Handler {

	r := &Handler{
		ctx: ctx,
		// socket: client,
		eventSource: events,
		// AMI: ami,
	}
	return r
}

//Handler ...
type Handler struct {
	// socket *ami.Socket
	eventSource *eventsbroker.EventsBroker
	ctx         context.Context
}

// Routes define las rutas para el recurso
func (h Handler) Routes() chi.Router {
	r := chi.NewRouter()
	r.Get("/", h.events)

	return r
}

//events Envía eventos de asterisk mediante sse
func (h Handler) events(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "Events").Logger()
	log.Debug().Msg("Inciando instancia de eventos sse")

	events := h.eventSource.Subscribe()
	defer func() {
		h.eventSource.Unsubscribe <- events
	}()

	flusher, ok := w.(http.Flusher)
	if !ok {
		log.Error().Msg("Error al crear Flusher")
		return
	}

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Transfer-Encoding", "chunked")

	ctx := r.Context()

	for {
		var response ami.Response
		select {
		case <-ctx.Done():
			log.Trace().Msg("Finalizada sesión sse")
			return
		case <-time.After(time.Second * 60):
			response = ami.Response{
				"Event": append(response["Event"], "KeepAlive"),
			}
		case response = <-events:
		}
		rsp := make(map[string]string)
		for k := range response {
			rsp[k] = strings.Join(response[k], ",")
		}
		e := response.Get("Event")
		//sólo envío los eventos. Ignoro las respuestas a comandos
		if e != "" {
			js, _ := json.Marshal(rsp)
			fmt.Fprintf(w, "data: %s\n\n", js)
			flusher.Flush()
		}

	}
}
