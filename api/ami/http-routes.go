package ami

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	_log "github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/asterisk/goami/ami"

	"gitlab.com/mind-framework/core/mind-core-api/render"
)

var log = _log.With().Str("pkg", "commands").Logger()

//NewHandler devuelve una nueva instancia del handler para este recurso
func NewHandler(ctx context.Context, client *ami.Pool) *Handler {

	r := &Handler{
		ctx:      ctx,
		asterisk: client,
	}
	return r
}

//Handler ...
type Handler struct {
	asterisk *ami.Pool
	ctx      context.Context
}

// Routes define las rutas para el recurso
func (h Handler) Routes() chi.Router {
	r := chi.NewRouter()
	r.Route("/", func(r chi.Router) {
		r.Get("/", h.commandList)
	})
	r.Get("/core-status", h.getCoreStatus)
	r.Get("/core-version", h.getCoreVersion)
	r.Get("/core-sysinfo", h.getSysInfo)
	r.Get("/core-settings", h.getCoreSettings)
	r.Get("/core-show-channels", h.getCoreShowChannels)
	r.Get("/ping", h.getPing)
	r.Get("/sip-peers", h.getSipPeers)
	r.Route("/modules", func(r chi.Router) {
		r.Get("/", h.modulesList)
		r.Route("/reload", func(r chi.Router) {
			r.Post("/", h.moduleReload)
			r.Post("/{module}", h.moduleReload)
		})
	})

	r.Route("/trunks", func(r chi.Router) {
		r.Get("/", h.listTrunks)
		r.Post("/", h.createTrunk)
		r.Route("/{type}", func(r chi.Router) {
			r.Get("/", h.listTrunks)
			r.Get("/{name}", h.getTrunk)
			r.Patch("/{name}", h.updateTrunk)
			r.Delete("/{name}", h.deleteTrunk)
		})
	})

	r.Route("/dialplan", func(r chi.Router) {
		r.Get("/", h.listDialplanContexts)
		r.Get("/{name}", h.getDialplanExtension)
		r.Post("/", h.createDialplanExtension)
		r.Patch("/{name}", h.updateDialplanContext)
		r.Delete("/{name}", h.deleteDialplanContext)
	})


	r.Route("/users", func(r chi.Router) {
		r.Get("/", h.listUsers)
		r.Get("/{name}", h.getUser)
		r.Patch("/{name}", h.updateUser)
		r.Delete("/{name}", h.deleteUser)
		r.Post("/", h.createUser)
	})

	r.Route("/playlists", func(r chi.Router) {
		r.Get("/", h.listPlayLists)
		r.Get("/{name}", h.getPlayList)
		r.Post("/", h.createPlayList)
		r.Patch("/{name}", h.updatePlayList)
		r.Delete("/{name}", h.deletePlayList)
	})

	r.Route("/announces", func(r chi.Router) {
		r.Get("/", h.listAnnounces)
		r.Get("/{name}", h.getAnnounce)
		r.Post("/", h.createAnnounce)
		r.Patch("/{name}", h.updateAnnounce)
		r.Delete("/{name}", h.deleteAnnounce)
	})

	r.Route("/config-files", func(r chi.Router) {
		r.Get("/", h.listConfigFiles)
		r.Route("/{filename}", func(r chi.Router) {
			r.Get("/", h.getConfig)
			r.Get("/categories", h.getCategories)
			r.Get("/{category}", h.getConfig)
			r.Patch("/{category}", h.updateConfig)
			r.Post("/", h.createCategory)
			r.Delete("/{category}", h.deleteCategory)
		})

	})

	return r
}

func (h Handler) commandList(w http.ResponseWriter, r *http.Request) {
	list := make(map[string]string)
	list["GET /ami/core-status"] = "Show PBX core status variables"
	list["GET /ami/core-settings"] = "Show PBX core settings"
	list["GET /ami/core-show-channels"] = "List currently active channels"
	list["GET /ami/ping"] = "Checks if Asterisk is Up"
	list["GET /ami/sip-peers"] = "List SIP peers"
	list["GET /ami/config-files/<filename>"] = "Get config file from asterisk"
	list["GET /ami/config-files/<folder>/<filename>"] = "Get config file from asterisk from the specified folder"
	list["GET /ami/config-files/<folder>/<filename>/categories"] = "Get the list of categories in specified conf file"
	list["GET /ami/modules"] = "List Modules"
	list["POST /ami/modules/reload/<module>"] = "Reload Module"
	render.Render(w, r, render.NewSuccessResponse(list))

}
