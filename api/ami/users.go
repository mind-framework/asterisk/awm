package ami

import (
	// "encoding/json"
	"net/http"
	//"os"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

//User contiene información sobre la troncal sip o iax
type User struct {
	AsteriskContext
}

func (h Handler) listUsers(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rsp := make([]map[string]string, 0)

	filename := "users.conf"

	uuid, _ := ami.GetUUID()
	response, err := ami.ListCategories(h.ctx, asterisk, uuid, filename)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	for i, user := range response {
		name := user[0]
		if strings.HasPrefix(i, "Category-") && name != "general" {
			rsp = append(rsp, map[string]string{"id": name})
		}
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}

//getUser devuelve la configuración de la troncal especificada
func (h Handler) getUser(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	filename := "users.conf"
	ext := chi.URLParam(r, "name")

	uuid, _ := ami.GetUUID()
	response, err := ami.GetConfig(h.ctx, asterisk, uuid, filename, ext, "")
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	contextos := AMI2Contexts(response)
	if len(contextos) == 0 {
		render.Render(w, r, render.NewGenericErrorResponse("El usuario especificada no pudo ser encontrada"))
		return
	}

	config := User{
		AsteriskContext: *contextos[0],
	}
	render.Render(w, r, render.NewSuccessResponse(config))

}

func (h Handler) updateUser(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	payload := rp.RequestBody.Data.(map[string]interface{})

	src := "users.conf"
	dst := src

	userID := chi.URLParam(r, "name")
	newName := payload["category"].(string)

	uuid, _ := ami.GetUUID()
	//Intento crear el archivo de configuración
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	//borro la categoría, si existe
	action := ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: userID,
	}

	uuid, _ = ami.GetUUID()
	ami.UpdateConfig(h.ctx, asterisk, uuid, src, src, false, action)

	actions := make([]ami.UpdateConfigAction, 0)

	userID = newName
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: userID,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: userID,
				Var:      partes[0],
				Value:    partes[1],
			})
		}

	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))

}

func (h Handler) createUser(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	users := rp.RequestBody.Data.([]interface{})
	payload := users[0].(map[string]interface{})

	src := "users.conf"
	dst := src

	userID := payload["category"].(string)

	uuid, _ := ami.GetUUID()
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: userID,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: userID,
				Var:      partes[0],
				Value:    partes[1],
			})
		}

	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))

}

func (h Handler) deleteUser(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	dst := "users.conf"
	userID := chi.URLParam(r, "name")
	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: userID,
	})

	uuid, _ := ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}
