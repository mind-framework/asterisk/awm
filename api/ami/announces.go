package ami

import (
	// "encoding/json"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

//Announce contiene información sobre la el Anuncio
type Announce struct {
	AsteriskContext
}

func (h Handler) listAnnounces(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rsp := []*Announce{}

	filename := "extensions.conf"

	uuid, _ := ami.GetUUID()
	response, err := ami.ListCategories(h.ctx, asterisk, uuid, filename)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	for _, pl := range response {
		name := pl[0]
		if strings.HasPrefix(name, "ann-") {
			rsp = append(rsp, &Announce{
				AsteriskContext{Category: name},
			})
		}
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}

//getAnnounce devuelve la configuración de la troncal especificada
func (h Handler) getAnnounce(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	filename := "extensions.conf"
	plName := chi.URLParam(r, "name")

	uuid, _ := ami.GetUUID()
	response, err := ami.GetConfig(h.ctx, asterisk, uuid, filename, plName, "")
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	contextos := AMI2Contexts(response)
	if len(contextos) == 0 {
		render.Render(w, r, render.NewGenericErrorResponse("El Anuncio especificad no pudo ser encontrado"))
		return
	}

	config := Announce{
		AsteriskContext: *contextos[0],
	}
	render.Render(w, r, render.NewSuccessResponse(config))
}

func (h Handler) updateAnnounce(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	payload := rp.RequestBody.Data.(map[string]interface{})

	src := "extensions.conf"
	dst := os.Getenv("ASTERISK_DIALPLAN_FILE")
	if dst == "" {
		dst = src
	}

	name := chi.URLParam(r, "name")
	newName := payload["category"].(string)
	if !strings.HasPrefix(newName, "ann-") {
		newName = "ann-" + newName
	}

	uuid, _ := ami.GetUUID()
	//Intento crear el archivo de configuración
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	//borro la categoría, si existe
	action := ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: name,
	}
	uuid, _ = ami.GetUUID()
	ami.UpdateConfig(h.ctx, asterisk, uuid, src, src, false, action)

	actions := make([]ami.UpdateConfigAction, 0)

	name = newName
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: name,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: name,
				Var:      partes[0],
				Value:    partes[1],
			})
		}
	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func (h Handler) createAnnounce(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	resourceList := rp.RequestBody.Data.([]interface{})
	payload := resourceList[0].(map[string]interface{})

	src := "extensions.conf"
	dst := os.Getenv("ASTERISK_DIALPLAN_FILE")
	if dst == "" {
		dst = src
	}

	newName := payload["category"].(string)
	if !strings.HasPrefix(newName, "ann-") {
		newName = "ann-" + newName
	}

	//Intento crear el archivo de configuración
	uuid, _ := ami.GetUUID()
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: newName,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: newName,
				Var:      partes[0],
				Value:    partes[1],
			})
		}

	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func (h Handler) deleteAnnounce(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	dst := "extensions.conf"
	name := chi.URLParam(r, "name")
	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: name,
	})

	uuid, _ := ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}
