package ami

import (
	// "encoding/json"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/go-chi/chi"
	// "github.com/rs/zerolog/log"

	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

//DialplanContext contiene información un contexto de dialplan
type DialplanContext struct {
	AsteriskContext
}

func (h Handler) listDialplanContexts(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rsp := []*DialplanContext{}

	filename := "extensions.conf"

	uuid, _ := ami.GetUUID()
	response, err := ami.ListCategories(h.ctx, asterisk, uuid, filename)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	for k, pl := range response {
		name := pl[0]
		if strings.Contains(k, "Category-") {
			rsp = append(rsp, &DialplanContext{
				AsteriskContext{Category: name},
			})
		}
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}

//getDialplanExtension devuelve la configuración de la troncal especificada
func (h Handler) getDialplanExtension(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	filename := "extensions.conf"
	plName := chi.URLParam(r, "name")

	uuid, _ := ami.GetUUID()
	response, err := ami.GetConfig(h.ctx, asterisk, uuid, filename, plName, "")
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	contextos := AMI2Contexts(response)
	if len(contextos) == 0 {
		render.Render(w, r, render.NewGenericErrorResponse("la extensión de dialplan especificada no pudo ser encontrada"))
		return
	}

	config := DialplanContext{
		AsteriskContext: *contextos[0],
	}
	render.Render(w, r, render.NewSuccessResponse(config))
}

func (h Handler) updateDialplanContext(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	payload := rp.RequestBody.Data.(map[string]interface{})

	src := "extensions.conf"
	dst := os.Getenv("ASTERISK_DIALPLAN_FILE")
	if dst == "" {
		dst = src
	}

	name := chi.URLParam(r, "name")
	newName := payload["category"].(string)

	uuid, _ := ami.GetUUID()
	//Intento crear el archivo de configuración
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	//borro la categoría, si existe
	action := ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: name,
	}
	uuid, _ = ami.GetUUID()
	ami.UpdateConfig(h.ctx, asterisk, uuid, src, src, false, action)

	actions := make([]ami.UpdateConfigAction, 0)

	name = newName
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: name,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		regex := regexp.MustCompile(`^\s*(\w+)\s*=>?\s*(.+)$`)
		partes := regex.FindStringSubmatch(o.(string))
		if len(partes) > 1 {
			newItem := ami.UpdateConfigAction{
				Action:   "Append",
				Category: name,
				Var:      partes[1],
				Value:    partes[2],
			}
			log.Trace().Interface("newItem", newItem).Msg("parseando contexto")
			actions = append(actions, newItem)
		}
	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func (h Handler) createDialplanExtension(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	resourceList := rp.RequestBody.Data.([]interface{})
	payload := resourceList[0].(map[string]interface{})

	src := "extensions.conf"
	dst := os.Getenv("ASTERISK_DIALPLAN_FILE")
	if dst == "" {
		dst = src
	}

	newName := payload["category"].(string)

	//Intento crear el archivo de configuración
	uuid, _ := ami.GetUUID()
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: newName,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		regex := regexp.MustCompile(`^\s*(\w+)\s*=>?\s*(.+)$`)
		partes := regex.FindStringSubmatch(o.(string))
		// partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: newName,
				Var:      partes[1],
				Value:    partes[2],
			})
		}

	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func (h Handler) deleteDialplanContext(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	dst := "extensions.conf"
	name := chi.URLParam(r, "name")
	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: name,
	})

	uuid, _ := ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}
