package ami

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

func (h Handler) getCategories(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	filename := chi.URLParam(r, "filename")
	uuid, _ := ami.GetUUID()
	response, err := ami.ListCategories(h.ctx, asterisk, uuid, filename)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	var rsp []string
	for k := range response {
		if strings.Contains(k, "Category-") {
			rsp = append(rsp, response[k][0])
		}
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}
