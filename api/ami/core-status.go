package ami

import (
	"net/http"
	"strings"

	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

func (h Handler) getCoreStatus(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		h.asterisk.Close(asterisk, true)
		return
	}
	uuid, _ := ami.GetUUID()
	response, err := ami.CoreStatus(h.ctx, asterisk, uuid)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	log.Trace().Interface("response", response).Send()
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	rsp := make(map[string]string)
	for k := range response {
		rsp[k] = strings.Join(response[k], ",")
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}

func (h Handler) getCoreVersion(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	uuid, _ := ami.GetUUID()
	response, err := ami.Command(h.ctx, asterisk, uuid, "core show version")
	if err != nil {
		log.Error().Err(err).Msg("Error al Obtener lista de archivos")
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(response.Get("Output")))
}

func (h Handler) getSysInfo(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	uuid, _ := ami.GetUUID()
	response, err := ami.Command(h.ctx, asterisk, uuid, "core show sysinfo")
	if err != nil {
		log.Error().Err(err).Msg("Error al Obtener lista de archivos")
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	res := make(map[string]string)
	for _, r := range response["Output"] {
		partes := strings.Split(r, ":")
		if len(partes) < 2 {
			continue
		}
		k := strings.Trim(partes[0], " ")
		value := strings.Trim(partes[1], " ")
		res[k] = value
	}

	render.Render(w, r, render.NewSuccessResponse(res))
}
