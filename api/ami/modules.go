package ami

import (
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/go-chi/chi"

	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

type moduleInfo struct {
	Module       string `json:"module"`
	Description  string `json:"description"`
	UseCount     int    `json:"useCount"`
	Status       string `json:"status"`
	SupportLevel string `json:"supportLevel"`
}

func (h Handler) moduleReload(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	module := chi.URLParam(r, "module")
	uuid, _ := ami.GetUUID()
	response, err := ami.Reload(h.ctx, asterisk, uuid, module)

	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	rsp := make(map[string]string)
	for k := range response {
		rsp[k] = strings.Join(response[k], ",")
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}

func (h Handler) modulesList(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	uuid, _ := ami.GetUUID()
	response, err := ami.Command(h.ctx, asterisk, uuid, "module show")
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	var rsp []moduleInfo
	expr := regexp.MustCompile("^([\\w.]+)\\s+(.+?)\\s+(\\d+)\\s+(Not Running|Running)\\s+([\\w]+)$")
	for _, v := range response["Output"] {

		matchs := expr.FindStringSubmatch(v)
		if matchs == nil {
			continue
		}
		useCount, _ := strconv.Atoi(matchs[3])
		rsp = append(rsp, moduleInfo{
			Module:       matchs[1],
			Description:  matchs[2],
			UseCount:     useCount,
			Status:       matchs[4],
			SupportLevel: matchs[5],
		})
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}
