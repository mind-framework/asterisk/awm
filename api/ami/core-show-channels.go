package ami

import (
	"net/http"

	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

func (h Handler) getCoreShowChannels(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	uuid, _ := ami.GetUUID()
	response, err := ami.CoreShowChannels(h.ctx, asterisk, uuid)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(parseList(response)))

}
