package ami

import (
	"sort"
	"strings"

	"gitlab.com/mind-framework/asterisk/goami/ami"
)

//parseList devuelve transforma un slice de Resuestas de AMI a un array de maps
// se utiliza para devolver el resultado en formato JSON
func parseList(response []ami.Response) []map[string]string {
	var rsp []map[string]string

	for i := range response {
		l := make(map[string]string)
		for k := range response[i] {
			l[k] = strings.Join(response[i][k], ",")
		}
		rsp = append(rsp, l)
	}
	return rsp
}

//ConfigToMap convierte una configuración recibida como respuesta de AMI
// a un map con el formato map[string][]string
// @example
// {
//		"categoria":
//				[
//					"exten=100,1,Answer()",
//					"..."
//				]
//
//
// }
// }
func configToMap(response ami.Response) map[string][]string {
	res := make(map[string][]string)
	cats := make(map[string]string)

	var keys []string
	for k := range response {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		partes := strings.Split(k, "-")
		switch partes[0] {
		case "Category":
			cats[partes[1]] = response[k][0]
		case "Line":
			cat := cats[partes[1]]
			res[cat] = append(res[cat], response[k]...)
		}
	}

	return res
}

// func configToSlice(response ami.Response) map[string][]string {

// 	res := []
// 	cats := make(map[string]string)

// 	var keys []string
// 	for k := range response {
// 		keys = append(keys, k)
// 	}

// 	sort.Strings(keys)

// 	for _, k := range keys {
// 		partes := strings.Split(k, "-")
// 		switch partes[0] {
// 		case "Category":
// 			cats[partes[1]] = response[k][0]
// 		case "Line":
// 			cat := cats[partes[1]]
// 			res[cat] = append(res[cat], response[k]...)
// 		}
// 	}

// 	return res
// }
