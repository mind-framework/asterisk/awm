package ami

import (
	// "encoding/json"
	"net/http"
	"regexp"
	"sort"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

//listConfigFiles devuelve la lista de archivos de configuración de la carpeta de configuración de asterisk
func (h Handler) listConfigFiles(w http.ResponseWriter, r *http.Request) {

	fileMap := make(map[string]bool)
	res := make([]string, 0)
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	uuid, _ := ami.GetUUID()
	response, err := ami.Command(h.ctx, asterisk, uuid, "config list")
	if err != nil {
		log.Error().Err(err).Msg("Error al Obtener lista de archivos")
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	for _, f := range response["Output"] {
		regex := regexp.MustCompile(`^\s*(\w+)\s+.*?(\w+)/(\w+\.conf)$`)
		partes := regex.FindStringSubmatch(f)
		if partes != nil {
			// module := partes[1]
			file := partes[3]
			if partes[2] != "asterisk" {
				continue
				// file = partes[2] + "/" + file
			}
			fileMap[file] = true
		}
	}
	for f := range fileMap {
		res = append(res, f)
	}

	sort.Strings(res)

	render.Render(w, r, render.NewSuccessResponse(res))
}

func (h Handler) getConfig(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	folder := chi.URLParam(r, "folder")
	filename := chi.URLParam(r, "filename")
	cat := chi.URLParam(r, "category")
	if folder != "" {
		filename = folder + "/" + filename
	}

	uuid, _ := ami.GetUUID()
	response, err := ami.GetConfig(h.ctx, asterisk, uuid, filename, cat, "")
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	config := AMI2Contexts(response)
	render.Render(w, r, render.NewSuccessResponse(config))
}

//updateConfig actualiza un archivo de configuración
//!NOTA: 	Este comando ejecuta ami.UpdateConfig por cada línea recibida.
//!				Esto no es lo más optimo pero evita error "too many lines" devuelto por AMI con configuraciones grandes
func (h Handler) updateConfig(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		h.asterisk.Close(asterisk, true)
		return
	}
	filename := chi.URLParam(r, "filename")
	category := chi.URLParam(r, "category")

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	payload := rp.RequestBody.Data.(map[string]interface{})
	newCatName := payload["category"].(string)
	reload := false
	paramReload := payload["reload"]
	if paramReload != nil {
		reload = paramReload.(bool)
	}
	log.Trace().Interface("reload", reload).Msg("actualizando contexto")
	actions := make([]ami.UpdateConfigAction, 0)

	oldCatName := category
	if newCatName != "" {
		category = newCatName
	}
	catTmp := category + "_tmp"
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: catTmp,
	})
	uuid, _ := ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, filename, filename, false, actions...)
	if err != nil {
		log.Error().Err(err).Msg("Error al ejecutar UpdateCOnfig")
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		log.Error().Interface("error", response["Message"]).Msg("Error al ejecutar UpdateCOnfig")
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	options := payload["options"].([]interface{})
	for _, o := range options {
		// partes := strings.Split(o.(string), "=")
		regex := regexp.MustCompile(`^\s*(\w+)\s*=>?\s*(.+)$`)
		partes := regex.FindStringSubmatch(o.(string))

		if partes != nil {
			action := ami.UpdateConfigAction{
				Action:   "Append",
				Category: catTmp,
				Var:      partes[1],
				Value:    partes[2],
			}
			uuid, _ := ami.GetUUID()
			response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, filename, filename, false, action)
			if err != nil {
				log.Error().Err(err).Msg("Error al ejecutar UpdateCOnfig")
				render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
				return
			}
			if response.Get("Response") != "Success" {
				log.Error().Interface("error", response["Message"]).Msg("Error al ejecutar UpdateCOnfig")
				render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
				return
			}
		}
	}

	actions = make([]ami.UpdateConfigAction, 0)
	//Si todo salió bien, elimina la categoria inicial y renombro la nueva
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: oldCatName,
	})

	actions = append(actions, ami.UpdateConfigAction{
		Action:   "RenameCat",
		Category: catTmp,
		Var:      "category",
		Value:    category,
	})

	uuid, _ = ami.GetUUID()
	response, err = ami.UpdateConfig(h.ctx, asterisk, uuid, filename, filename, reload, actions...)
	if err != nil {
		log.Error().Err(err).Msg("Error al ejecutar UpdateCOnfig")
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		log.Error().Interface("error", response["Message"]).Msg("Error al ejecutar UpdateCOnfig")
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	h.getConfig(w, r)
	return

	// render.Render(w, r, render.NewSuccessResponse(""))

}

//createCategory crea una categoría en el archivos de configuración especificado
//!NOTA: este comando ejecuta ami.UpdateConfig por cada línea recibida.
//! 		 esto no es lo más optimo pero evita error "too many lines" devuelto por AMI con configuraciones grandes
func (h Handler) createCategory(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	filename := chi.URLParam(r, "filename")

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	cats := rp.RequestBody.Data.([]interface{})

	for _, c := range cats {
		payload := c.(map[string]interface{})
		category := payload["category"].(string)
		action := ami.UpdateConfigAction{
			Action:   "NewCat",
			Category: category,
		}
		uuid, _ := ami.GetUUID()
		response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, filename, filename, false, action)
		if err != nil {
			render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
			return
		}
		if response.Get("Response") != "Success" {
			render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
			return
		}
		if payload["options"] == nil {
			continue
		}
		options := payload["options"].([]interface{})
		for _, o := range options {
			partes := strings.Split(o.(string), "=")
			if len(partes) > 1 {
				action = ami.UpdateConfigAction{
					Action:   "Append",
					Category: category,
					Var:      partes[0],
					Value:    partes[1],
				}
				uuid, _ := ami.GetUUID()
				response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, filename, filename, false, action)
				if err != nil {
					render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
					return
				}
				if response.Get("Response") != "Success" {
					render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
					return
				}
			}
		}
	}

	render.Render(w, r, render.NewSuccessResponse(""))

}

func (h Handler) deleteCategory(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	filename := chi.URLParam(r, "filename")
	category := chi.URLParam(r, "category")

	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: category,
	})

	uuid, _ := ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, filename, filename, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))

}

//AsteriskContext contains basic asterisk context data
type AsteriskContext struct {
	Category string    `json:"category"`
	Options  *[]string `json:"options,omitempty"`
}

//AMI2Contexts receives a AMI response and returns an slice of contexts
func AMI2Contexts(response ami.Response) []*AsteriskContext {

	// res := make(map[string][]string)
	cats := make(map[string]string)
	index := make(map[string]*AsteriskContext)

	config := make([]*AsteriskContext, 0)
	var keys []string
	for k := range response {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		partes := strings.Split(k, "-")
		if len(partes) < 2 {
			continue
		}
		i := partes[1]

		switch partes[0] {
		case "Category":
			cat := response[k][0]
			cats[i] = cat
			index[cat] = &AsteriskContext{
				Category: cat,
				Options:  &[]string{},
			}
		case "Line":
			cat := cats[i]
			*index[cat].Options = append(*index[cat].Options, response[k][0])
		}
	}

	for _, v := range index {
		config = append(config, v)
	}

	return config
}
