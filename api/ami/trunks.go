package ami

import (
	// "encoding/json"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

//Trunk contiene información sobre la troncal sip o iax
type Trunk struct {
	Type string `json:"type"`
	// Name string `json:"name"`
	AsteriskContext
}

func (h Handler) listTrunks(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rsp := []*Trunk{}

	trunkType := chi.URLParam(r, "type")
	allowedTypes := []string{"sip", "iax"}
	invalidTrunkType := true
	for _, t := range allowedTypes {
		if t == trunkType || trunkType == "" {
			invalidTrunkType = false
			filename := t + ".conf"

			uuid, _ := ami.GetUUID()
			response, err := ami.ListCategories(h.ctx, asterisk, uuid, filename)
			if err != nil {
				h.asterisk.Close(asterisk, true)
				render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
				return
			}
			log.Trace().Interface("response", response).Msg("")
			if response.Get("Response") != "Success" {
				render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
				return
			}
			for _, trunk := range response {
				name := trunk[0]
				if strings.HasPrefix(name, "trunk-") {
					rsp = append(rsp, &Trunk{t,
						AsteriskContext{Category: name},
					})
				}
			}
		}
	}
	if invalidTrunkType {
		render.Render(w, r, render.NewGenericErrorResponse("Invalid trunk type"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}

//getTrunk devuelve la configuración de la troncal especificada
func (h Handler) getTrunk(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	trunkType := chi.URLParam(r, "type")
	filename := trunkType + ".conf"
	trunkName := chi.URLParam(r, "name")

	uuid, _ := ami.GetUUID()
	response, err := ami.GetConfig(h.ctx, asterisk, uuid, filename, trunkName, "")
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	contextos := AMI2Contexts(response)
	if len(contextos) == 0 {
		render.Render(w, r, render.NewGenericErrorResponse("La troncal especificada no pudo ser encontrada"))
		return
	}

	config := Trunk{
		Type:            trunkType,
		AsteriskContext: *contextos[0],
	}
	render.Render(w, r, render.NewSuccessResponse(config))

}

//TODO: agregar soporte para cambio de tipo de troncal
func (h Handler) updateTrunk(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	payload := rp.RequestBody.Data.(map[string]interface{})

	trunkType := chi.URLParam(r, "type")
	src := trunkType + ".conf"
	dst := ""
	// dst = "custom/" + trunkType + fileSufix + ".conf"
	switch trunkType {
	case "sip":
		dst = os.Getenv("ASTERISK_SIP_TRUNK_FILE")
		if dst == "" {
			dst = src
		}
	case "iax":
		dst = os.Getenv("ASTERISK_IAX_TRUNK_FILE")
		if dst == "" {
			dst = src
		}
	}

	trunkName := chi.URLParam(r, "name")
	newName := payload["category"].(string)
	if !strings.HasPrefix(newName, "trunk-") {
		newName = "trunk-" + newName
	}

	uuid, _ := ami.GetUUID()
	//Intento crear el archivo de configuración
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	//borro la categoría, si existe
	action := ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: trunkName,
	}
	uuid, _ = ami.GetUUID()
	ami.UpdateConfig(h.ctx, asterisk, uuid, src, src, false, action)

	actions := make([]ami.UpdateConfigAction, 0)

	trunkName = newName
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: trunkName,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: trunkName,
				Var:      partes[0],
				Value:    partes[1],
			})
		}

	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))

}

func (h Handler) createTrunk(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	rp := middlewares.GetContextParams(r)
	log.Trace().Interface("data", rp).Msg("requestParams")
	trunks := rp.RequestBody.Data.([]interface{})
	payload := trunks[0].(map[string]interface{})

	trunkType := payload["type"].(string)
	src := trunkType + ".conf"
	var dst string
	// dst := "custom/" + trunkType + "_trunks.conf"

	switch trunkType {
	case "sip":
		dst = os.Getenv("ASTERISK_SIP_TRUNK_FILE")
		if dst == "" {
			dst = src
		}
	case "iax":
		dst = os.Getenv("ASTERISK_IAX_TRUNK_FILE")
		if dst == "" {
			dst = src
		}
	}

	trunkName := payload["category"].(string)

	if !strings.HasPrefix(trunkName, "trunk-") {
		trunkName = "trunk-" + trunkName
	}

	uuid, _ := ami.GetUUID()
	//Intento crear el archivo de configuración
	ami.CreateConfig(h.ctx, asterisk, uuid, dst)

	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "NewCat",
		Category: trunkName,
	})

	options := payload["options"].([]interface{})
	for _, o := range options {
		partes := strings.Split(o.(string), "=")
		if len(partes) > 1 {
			actions = append(actions, ami.UpdateConfigAction{
				Action:   "Append",
				Category: trunkName,
				Var:      partes[0],
				Value:    partes[1],
			})
		}

	}

	uuid, _ = ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))

}

func (h Handler) deleteTrunk(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	trunkType := chi.URLParam(r, "type")
	dst := trunkType + ".conf"
	trunkName := chi.URLParam(r, "name")
	actions := make([]ami.UpdateConfigAction, 0)
	actions = append(actions, ami.UpdateConfigAction{
		Action:   "DelCat",
		Category: trunkName,
	})

	uuid, _ := ami.GetUUID()
	response, err := ami.UpdateConfig(h.ctx, asterisk, uuid, dst, dst, false, actions...)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}
