package ami

import (
	"net/http"
	"strings"

	"gitlab.com/mind-framework/asterisk/goami/ami"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

func (h Handler) getCoreSettings(w http.ResponseWriter, r *http.Request) {
	asterisk, err := h.asterisk.GetSocket()
	defer h.asterisk.Close(asterisk, false)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	uuid, _ := ami.GetUUID()
	response, err := ami.CoreSettings(h.ctx, asterisk, uuid)
	if err != nil {
		h.asterisk.Close(asterisk, true)
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	if response.Get("Response") != "Success" {
		render.Render(w, r, render.NewGenericErrorResponse(response.Get("Message")))
		return
	}
	rsp := make(map[string]string)
	for k := range response {
		rsp[k] = strings.Join(response[k], ",")
		// log.Debug().Interface("k", k).Msg("")
	}

	render.Render(w, r, render.NewSuccessResponse(rsp))

}
