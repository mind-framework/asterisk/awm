package sounds

import (
	"context"
	"fmt"
	"io"
	"mime"
	"os"
	"path"
	"regexp"
	"strings"
	"time"
	"os/exec"
	"io/ioutil"
	"net/http"

	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"

	"github.com/go-chi/chi"
	_log "github.com/rs/zerolog/log"

)

var log = _log.With().Str("pkg", "sse").Logger()

//NewHandler devuelve una nueva instancia del handler para este recurso
func NewHandler(ctx context.Context, folder string) *Handler {

	r := &Handler{
		ctx:          ctx,
		soundsFolder: folder,
	}
	return r
}

//Handler ...
type Handler struct {
	soundsFolder string
	ctx          context.Context
}

// Routes define las rutas para el recurso
func (h Handler) Routes() chi.Router {
	r := chi.NewRouter()
	r.Get("/", h.getSoundList)
	r.Get("/{name}", h.getSound)
	r.Get("/{folder}/{name}", h.getSound)
	r.Post("/", h.postSound)
	r.Post("/{folder}/", h.postSound)
	r.Delete("/{name}", h.deleteSound)
	r.Delete("/{folder}/{name}", h.deleteSound)

	return r
}

//getFiles Devuelve la lista de archivos de audio de la carpeta especificada
func (h Handler) getSoundList(w http.ResponseWriter, r *http.Request) {
	log = log.With().Str("ctx", "Sounds").Logger()
	filter := r.URL.Query().Get("name")
	folder := r.URL.Query().Get("folder")
	res, err := getRecursiveFileList(h.soundsFolder+"/"+folder, filter, folder, true)
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(res))
}

//devuelve el archivo de audio indicado
func (h Handler) getSound(w http.ResponseWriter, r *http.Request) {
	folder := chi.URLParam(r, "folder")
	if folder != "" {
		folder = folder + "/"
	}
	name := chi.URLParam(r, "name")
	fullname := h.soundsFolder + "/" + folder + name
	log.Trace().Str("fullname", fullname).Msg("solicitada descarga de audio")
	t := mime.TypeByExtension(strings.ToLower(path.Ext(fullname)))
	if t != "" {
		w.Header().Add("Content-Type", t)
	} else {
		w.Header().Add("Content-Type", "application/octet-stream")

	}
	http.ServeFile(w, r, fullname)
}

func (h Handler) postSound(w http.ResponseWriter, r *http.Request) {
	rp := middlewares.GetContextParams(r)
	fmt.Printf("%#v", rp)

	if len(rp.Files) == 0 {
		log.Error().Msg("No se recibieron archivos")
		render.Render(w, r, render.NewGenericErrorResponse("No se recibieron archivos"))
		return
	}
	file := rp.Files[0]

	matched, _ := regexp.Match(`^[\w\d_-]+\.(sln\d*|wav|vox|gsm)+$`, []byte(file.Filename))
	if !matched {
		render.Render(w, r, render.NewGenericErrorResponse(file.Filename+": Tipo de Archivo no soportado"))
		return
	}

	folder := chi.URLParam(r, "folder")
	if folder != "" {
		folder = folder + "/"
	}
	path := h.soundsFolder + "/" + folder + "/"

	out, pathError := ioutil.TempFile(path, "tmp-")
	if pathError != nil {
		log.Error().Err(pathError).Msg("al crear archivo temporal")
		render.Render(w, r, render.NewGenericErrorResponse(pathError.Error()))
		return
	}
	defer out.Close()
	defer os.Remove(out.Name())
	log.Trace().Interface("tmpFile", out.Name()).Msg("Generando archivo temporal")

	_, copyError := io.Copy(out, file.File)
	if copyError != nil {
		log.Error().Err(copyError).Msg("Error al guardar archivo")
		render.Render(w, r, render.NewGenericErrorResponse(copyError.Error()))
		return
	}

	
	log.Trace().Msg("Convirtiendo audio")
	if err := convert(out.Name(), path+"/"+file.Filename); err != nil {
		log.Error().Err(err).Msg("Error al convertir audio")
		render.Render(w, r, render.NewGenericErrorResponse(copyError.Error()))
		return
	}


	// if err := os.Rename(out.Name(), path+"/"+file.Filename); err != nil {
	// 	log.Error().Err(err).Msg("Error al mover archivo")
	// 	render.Render(w, r, render.NewGenericErrorResponse(copyError.Error()))
	// 	return
	// }
	render.Render(w, r, render.NewSuccessResponse(""))
}

func (h Handler) deleteSound(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	folder := chi.URLParam(r, "folder")
	if folder != "" {
		folder = folder + "/"
	}
	fullname := h.soundsFolder + "/" + folder + name

	//TODO: Sanitizar name
	matched, err := regexp.Match(`^[\w\d_-]+\.(sln\d*|wav|vox|gsm)+$`, []byte(name))

	if !matched {
		render.Render(w, r, render.NewGenericErrorResponse("Nombre de Archivo de Audio No soportado ["+name+"]"))
		return
	}

	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}

	if err := os.Remove(fullname); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(err.Error()))
		return
	}
	render.Render(w, r, render.NewSuccessResponse("chau "+name))
}

//FileInfo Contiene Información del archivo de audio
type FileInfo struct {
	Folder  string    `json:"folder"`
	Name    string    `json:"name"`
	Size    int64     `json:"size"`
	ModTime time.Time `json:"modTime"`
	// Description *string   `json:"description"`
}

func getRecursiveFileList(path, filter, relativePath string, deep bool) ([]FileInfo, error) {
	res := make([]FileInfo, 0)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		name := file.Name()
		if filter != "" {
			if !strings.Contains(name, filter) {
				r, _ := getRecursiveFileList(path+"/"+name, filter, name, false)
				res = append(res, r...)
				continue
			}
		}
		log.Trace().Str("name", name).Interface("isdir", file.IsDir()).Msg("")
		if file.IsDir() {
			r, _ := getRecursiveFileList(path+"/"+name, "", name, false)
			res = append(res, r...)
		} else {
			matched, _ := regexp.Match(`^[\w\d_-]+\.(sln\d*|wav|vox|gsm)+$`, []byte(name))
			if !matched {
				continue
			}
			res = append(res, FileInfo{
				Folder:  relativePath,
				Name:    name,
				Size:    file.Size(),
				ModTime: file.ModTime(),
			})
		}

	}
	return res, nil

}


func convert(src string, dst string) error {
	// cmdLline := fmt.Sprintf("sox ")
	log.Trace().Str("src", src).Str("dst", dst).Msg("Convirtiendo audio")

    cmd := exec.Command("sox", src, "-r 8000", dst)

    return cmd.Run()
}