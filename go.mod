module gitlab.com/mind-framework/asterisk/awm

go 1.15

// replace gitlab.com/mind-framework/core/mind-core-api => ./submodules/mind-core-api

//reemplazo goami por la versión local
replace gitlab.com/mind-framework/asterisk/goami => ./submodules/goami

require (
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/render v1.0.1 // indirect
	github.com/rs/zerolog v1.20.0
	gitlab.com/mind-framework/asterisk/goami v1.2.0
	gitlab.com/mind-framework/core/mind-core-api v0.2.0
)
